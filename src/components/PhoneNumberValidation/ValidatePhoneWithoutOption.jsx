import  { useState, useEffect } from "react";
import {
  formatPhoneNumber,
  getCountryCallingCode,
  getCountries,
  isPossiblePhoneNumber,
  formatPhoneNumberIntl,
  isValidPhoneNumber,
} from "react-phone-number-input";
import Input from "react-phone-number-input/input";

const ValidatePhoneWithoutOption = () => {
    const [country, setCountry] = useState("");  
    const [countryCode, setCountryCode] = useState("");
    const [value, setValue] = useState("");
    const [validCountryCode, setValidCountryCode] = useState(true);

    // Get the list of countries and their calling codes
    const countries = getCountries();
    const callingCodes = countries.map(
        (country) => `+${getCountryCallingCode(country)}`
    );

    // Function to get the country based on the entered country code
    const getCountryFromCode = (code) => {
        const matchingCountry = countries.find(
            (country) => `+${getCountryCallingCode(country)}` === code
        );

        return matchingCountry ? matchingCountry : countries[0];
    };

    //  to check the validity of the entered country code
    useEffect(() => {
        setValidCountryCode(callingCodes.includes(countryCode));
    }, [countryCode, callingCodes]);

    // Handler for changing the country code
    const handleCountryCodeChange = (newCode) => {
        setCountryCode(newCode);
        const newCountry = getCountryFromCode(newCode);
        newCountry && setCountry(newCountry);
    };

    return (
        <div style={{ marginLeft: "50px" }}>
            <p style={{ marginBottom: "20px" }}>
                Phone Validation without Country Code dropdown
            </p>

            <div
                style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "300px",
                }}
            >
                {/* Input for entering the country code */}
                <Input
                    international
                    placeholder="+Code"
                    withCountryCallingCode={false}
                    value={countryCode}
                    onChange={handleCountryCodeChange}
                    style={{ width: "20%", padding: "15px", marginRight: "3px" }}
                />
                {/* Input for entering the phone number */}
                <Input
                    international
                    placeholder="Enter phone number"
                    withCountryCallingCode={false}
                    value={value}
                    onChange={setValue}
                    country={country}
                    style={{ width: "75%", padding: "15px" }}
                />
            </div>
            <div style={{ marginTop: "10px" }}>
                Country: {country && country} <br />
                Code: {countryCode && countryCode} {validCountryCode ? "(Valid)" : "(Invalid)"} <br />
                Is possible: {validCountryCode && value && isPossiblePhoneNumber(value) ? "true" : "false"} <br />
                Is valid: {validCountryCode && value && isValidPhoneNumber(value) ? "true" : "false"} <br />
                National: {value && formatPhoneNumber(value)} <br />
                International: {value && formatPhoneNumberIntl(value)} <br />
            </div>
        </div>
    );
};

export default ValidatePhoneWithoutOption;
