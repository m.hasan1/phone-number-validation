import { useEffect, useState } from "react";
import {
  formatPhoneNumber,
  getCountryCallingCode,
  getCountries,
  isPossiblePhoneNumber,
  formatPhoneNumberIntl,
  isValidPhoneNumber,
} from "react-phone-number-input";
import Input from "react-phone-number-input/input";

const ValidatePhoneWithoutISO = () => {
  const [country, setCountry] = useState("BD"); 
  const [code, setCode] = useState("+880"); 
  const [value, setValue] = useState();
  const [selected, setSelected] = useState(false); 

  // Handle country change event
  const handleCountryChange = (countryAlias) => {
    setCountry(countryAlias);
    setCode(getCountryCallingCode(countryAlias));
    setValue("");
    setSelected(true);
  };

  // Helper function to display country option in the select dropdown
  const getOptionDisplay = (country) =>
    `${country} +${getCountryCallingCode(country)}`;

  // to log changes in country, code, and value
  useEffect(() => {
    console.log("Country:", country, "Code:", code, "Value:", value);
  }, [country, code, value]);

  return (
    <div style={{ marginRight: "20px" }}>
      <p style={{ marginBottom: "20px" }}>Phone Validation without ISO</p>

      {/* Phone input section */}
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {/* Country selection dropdown */}
        <div
          style={{
            display: "flex",
            alignItems: "center",
            position: "relative",
            marginRight: "3px",
          }}
        >
          {/* Select dropdown for countries */}
          <select
            style={{ padding: "15px 3px", cursor: "pointer", width: "100%" }}
            value={code}
            onChange={(event) =>
              handleCountryChange(event.target.value || undefined)
            }
          >
            {/* Populate country options */}
            {getCountries().map((country) => (
              <option key={country} value={country}>
                {getOptionDisplay(country)}
              </option>
            ))}
          </select>

          {/* Display selected country code */}
          <div
            style={{
              fontSize: "14px",
              position: "absolute",
              top: "1px",
              left: "5px",
              background: "#fff",
              padding: "15px 20px",
            }}
          >
            {selected ? "+" + code : ""}
          </div>
        </div>

        {/* Phone input field */}
        <div style={{ width: "60%", marginLeft: "1px" }}>
          <Input
            placeholder="Enter phone number"
            country={country}
            international
            withCountryCallingCode={false}
            value={value}
            onChange={setValue}
            style={{ width: "100%", padding: "15px" }}
          />
        </div>
      </div>

      {/* Phone information */}
      <div
        style={{
          marginTop: "10px",
        }}
      >
        Country: {country && country} <br />
        Code: {code && code} <br />
        Is possible:{" "}
        {value && isPossiblePhoneNumber(value) ? "true" : "false"}<br />
        Is valid: {value && isValidPhoneNumber(value) ? "true" : "false"}<br />
        National: {value && formatPhoneNumber(value)}<br />
        International: {value && formatPhoneNumberIntl(value)}<br />
      </div>
    </div>
  );
};

export default ValidatePhoneWithoutISO;
