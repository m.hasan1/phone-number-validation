import { useEffect, useState } from "react";
import {
  formatPhoneNumber,
  getCountryCallingCode,
  getCountries,
  isPossiblePhoneNumber,
  formatPhoneNumberIntl,
  isValidPhoneNumber,
} from "react-phone-number-input";
import Input from "react-phone-number-input/input";

const ValidatePhoneWithISO = () => {
  const [country, setCountry] = useState("BD"); 
  const [code, setCode] = useState("+880"); 
  const [value, setValue] = useState(); 

  // Handle country change event
  const handleCountryChange = (countryAlias) => {
    setCountry(countryAlias);
    setCode(getCountryCallingCode(countryAlias));
    setValue("");
  };

  // to log changes in country, code, and value
  useEffect(() => {
    console.log("Country:", country, "Code:", code, "Value:", value);
  }, [country, code, value]);

  return (
    <div
      style={{
        marginRight: "20px",
      }}
    >
      <p
        style={{
          marginBottom: "20px",
        }}
      >
        Phone Validation with ISO
      </p>

      {/* Phone input section */}
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {/* Country selection dropdown */}
        <div
          style={{
            marginRight: "3px",
          }}
        >
          <select
            style={{
              padding: "15px 3px",
              cursor: "pointer",
            }}
            value={country}
            onChange={(event) =>
              handleCountryChange(event.target.value || undefined)
            }
          >
            {/* Populate country options */}
            {getCountries().map((country) => (
              <option key={country} value={country}>
                {country} +{getCountryCallingCode(country)}
              </option>
            ))}
          </select>
        </div>

        {/* Phone input field */}
        <div
          style={{
            width: "60%",
          }}
        >
          <Input
            country={country}
            international
            placeholder="Enter phone number"
            withCountryCallingCode={false}
            value={value}
            onChange={setValue}
            style={{ width: "100%", padding: "15px" }}
          />
        </div>
      </div>

      <div
        style={{
          marginTop: "10px",
        }}
      >
        Country: {country && country} <br />
        Code: {code && code} <br />
        Is possible:{" "}
        {value && isPossiblePhoneNumber(value) ? "true" : "false"}<br />
        Is valid: {value && isValidPhoneNumber(value) ? "true" : "false"}<br />
        National: {value && formatPhoneNumber(value)}<br />
        International: {value && formatPhoneNumberIntl(value)}<br />
      </div>
    </div>
  );
};

export default ValidatePhoneWithISO;
