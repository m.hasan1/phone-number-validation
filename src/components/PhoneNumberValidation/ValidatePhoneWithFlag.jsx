import { useState } from "react";
import PhoneInput, {
  formatPhoneNumber,
  formatPhoneNumberIntl,
  isValidPhoneNumber,
  isPossiblePhoneNumber,
} from "react-phone-number-input";

const ValidatePhoneWithFlag = () => {
  const [value, setValue] = useState();
 

  return (
    <div>
      <p style={{ marginBottom: "20px" }}>Phone Validation with Flag</p>
      <PhoneInput 
        placeholder="Enter phone number"
        defaultCountry="BD"
        value={value} 
        onChange={setValue}
        error={
          value
            ? isValidPhoneNumber(value)
              ? undefined
              : "Invalid phone number"
            : "Phone number required"
        }
      />

      <div style={{ marginTop: "10px" }}>
        Is possible: {value && isPossiblePhoneNumber(value) ? "true" : "false"} <br />
        Is valid: {value && isValidPhoneNumber(value) ? "true" : "false"}<br />
        National: {value && formatPhoneNumber(value)}<br />
        International: {value && formatPhoneNumberIntl(value)}<br />
      </div>
    </div>
  );
};

export default ValidatePhoneWithFlag;
