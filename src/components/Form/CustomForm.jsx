/* eslint-disable react/prop-types */
import { Box,  TextField } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

const CustomForm = ({ row, index, handleInputChange, handleDeleteRow }) => {
  return (
    <Box
      key={index}
      display="flex"
      alignItems="center"
      justifyContent="center"
      padding="20px"

    >
      <TextField
        id={`weight${index}`}
        label={`Weight ${index + 1}`}
        type="text"
        value={row.weight}
        onChange={(e) => handleInputChange(index, "weight", e.target.value)}
        sx={{
          margin: "5px",
        }}
      />
      <TextField
        id={`amount${index}`}
        label={`Amount ${index + 1}`}
        type="text"
        value={row.amount}
        onChange={(e) => handleInputChange(index, "amount", e.target.value)}
        sx={{
          margin: "5px",
        }}
      />
      <TextField
        id={`unit${index}`}
        label={`Unit ${index + 1}`}
        type="text"
        value={row.unit}
        onChange={(e) => handleInputChange(index, "unit", e.target.value)}
        sx={{
          margin: "5px",
        }}
      />
      <Box>
        <DeleteIcon
          color="error"
          sx={{ cursor: "pointer" }}
          onClick={() => handleDeleteRow(index)}
        />
      </Box>
    </Box>
  );
};

export default CustomForm;

/* eslint-disable react/prop-types */
// import { Box,  TextField } from "@mui/material";
// import DeleteIcon from "@mui/icons-material/Delete";

// const CustomForm = ({ row, index, handleInputChange, handleDeleteRow }) => {
//   return (
//     <Box
//       key={index}
//       display="flex"
//       alignItems="center"
//       justifyContent="center"
//       padding="20px"
//     >
//       <TextField
//         id={`weight${index}`}
//         label={`Weight ${index + 1}`}
//         type="text"
//         value={row.weight}
//         onChange={(e) => handleInputChange(index, "weight", e.target.value)}
//         sx={{
//           margin: "5px",
//         }}
//       />
//       <TextField
//         id={`amount${index}`}
//         label={`Amount ${index + 1}`}
//         type="text"
//         value={row.amount}
//         onChange={(e) => handleInputChange(index, "amount", e.target.value)}
//         sx={{
//           margin: "5px",
//         }}
//       />
//       <TextField
//         id={`unit${index}`}
//         label={`Unit ${index + 1}`}
//         type="text"
//         value={row.unit}
//         onChange={(e) => handleInputChange(index, "unit", e.target.value)}
//         sx={{
//           margin: "5px",
//         }}
//       />
//       <Box>
//         <DeleteIcon
//           color="error"
//           sx={{ cursor: "pointer" }}
//           onClick={() => handleDeleteRow(index)}
//         />
//       </Box>
//     </Box>
//   );
// };

// export default CustomForm;