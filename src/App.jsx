import "react-phone-number-input/style.css";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PhoneNumberValidation from "./pages/PhoneNumberValidation";
import CustomFormPage from "./pages/CustomFormPage";

function App() {
  return (
    <BrowserRouter>
    <Routes>
     
    <Route path="/phone-validation" element={<PhoneNumberValidation />} />
    <Route path="/" element={<CustomFormPage />} />

    </Routes>
    </BrowserRouter>
  );
}

export default App;
