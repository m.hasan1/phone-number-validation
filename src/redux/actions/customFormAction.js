
export const addRow = () => ({
  type: "ADD_ROW",
});

export const deleteRow = (index) => ({
  type: "DELETE_ROW",
  payload: { index },
});

export const updateRow = (index, field, value) => ({
  type: "UPDATE_ROW",
  payload: { index, field, value },
});
