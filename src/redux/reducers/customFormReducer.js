
const initialState = {
  rows: [{ weight: "", amount: "", unit: "" }],
};

const customFormReducer = (state = initialState, action) => {
  let updatedRows;

  switch (action.type) {
    case "ADD_ROW":
      return { rows: [...state.rows, { weight: "", amount: "", unit: "" }] };

    case "DELETE_ROW":
      updatedRows = [...state.rows];
      updatedRows.splice(action.payload.index, 1);
      return { rows: updatedRows };

    case "UPDATE_ROW":
      updatedRows = [...state.rows];
      updatedRows[action.payload.index][action.payload.field] = action.payload.value;
      return { rows: updatedRows };

    default:
      return state;
  }
};

export default customFormReducer;
