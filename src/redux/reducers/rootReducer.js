import { combineReducers } from 'redux';
import customFormReducer from './customFormReducer';

const rootReducer = combineReducers({
  customForm: customFormReducer,
});

export default rootReducer;
