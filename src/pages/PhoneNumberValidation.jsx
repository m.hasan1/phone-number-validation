import ValidatePhoneWithISO from '../components/PhoneNumberValidation/ValidatePhoneWithISO'
import ValidatePhoneWithoutISO from '../components/PhoneNumberValidation/ValidatePhoneWithoutISO'
import ValidatePhoneWithFlag from '../components/PhoneNumberValidation/ValidatePhoneWithFlag'
import ValidatePhoneWithoutOption from '../components/PhoneNumberValidation/ValidatePhoneWithoutOption'

const PhoneNumberValidation = () => {
  return (
    <>
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center",
      }}
    >
      <ValidatePhoneWithISO />
      <ValidatePhoneWithoutISO />
    </div>
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop:"100px"
      }}
    >
      <ValidatePhoneWithFlag />
      <ValidatePhoneWithoutOption />
    </div>
  </>
  )
}

export default PhoneNumberValidation