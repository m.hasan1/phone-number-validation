import { Box, Button } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import CustomForm from "../components/Form/CustomForm";
import { useDispatch, useSelector } from "react-redux";
import { addRow, deleteRow, updateRow } from "../redux/actions/customFormAction";

const CustomFormPage = () => {
  const dispatch = useDispatch();
  const rows = useSelector((state) => state.customForm.rows);
  console.log("Rows:",rows)

  const handleAddRow = () => {
    dispatch(addRow());
  };

  const handleDeleteRow = (index) => {
    console.log(index)
    dispatch(deleteRow(index));
  };

  const handleInputChange = (index, field, value) => {
    console.log(index)
    dispatch(updateRow(index, field, value));
  };
  return (
    <Box
      width="100%"
      height="100vh"
      display="flex"
      alignItems="center"
      justifyContent="center"
      overflow="scroll"
    >
      <Box   sx={{ minWidth: 500, backgroundColor: "#f3f3f3", marginTop:"5px" }}>
        {rows.map((row, index) => (
          <CustomForm
            key={index}
            row={row}
            index={index}
            handleInputChange={handleInputChange}
            handleDeleteRow={handleDeleteRow}
          />
        ))}
        <Box sx={{ marginLeft: "20px", paddingBottom: "20px" }}>
          <Button
            variant="contained"
            endIcon={<AddIcon />}
            onClick={handleAddRow}
          >
            Add Row
          </Button>
        </Box>
      </Box>
    </Box>
  );
};
export default CustomFormPage;
// import { useState } from "react";
// import { Box, Button } from "@mui/material";
// import AddIcon from "@mui/icons-material/Add";
// import CustomForm from "../components/Form/CustomForm";

// const CustomFormPage = () => {
//   const [rows, setRows] = useState([{ weight: "", amount: "", unit: "" }]);

//   const handleAddRow = () => {
//     setRows([...rows, { weight: "", amount: "", unit: "" }]);
//   };

//   const handleDeleteRow = (index) => {
//     const updatedRows = [...rows];
//     updatedRows.splice(index, 1);
//     setRows(updatedRows);
//     console.log("Current array:", updatedRows);
//   };

//   const handleInputChange = (index, field, value) => {
//     const updatedRows = [...rows];
//     updatedRows[index][field] = value;
//     setRows(updatedRows);
//     console.log("Current array:", updatedRows);
//   };

//   return (
//     <Box
//       width="100%"
//       height="100vh"
//       display="flex"
//       alignItems="center"
//       justifyContent="center"
//     >
//       <Box sx={{ minWidth: 500, backgroundColor: "#f3f3f3" }}>
//         {rows.map((row, index) => (
//           <CustomForm
//             key={index}
//             row={row}
//             index={index}
//             handleInputChange={handleInputChange}
//             handleDeleteRow={handleDeleteRow}
//           />
//         ))}
//         <Box sx={{ marginLeft: "20px", paddingBottom: "20px" }}>
//           <Button
//             variant="contained"
//             endIcon={<AddIcon />}
//             onClick={handleAddRow}
//           >
//             Add Row
//           </Button>
//         </Box>
//       </Box>
//     </Box>
//   );
// };

// export default CustomFormPage;
